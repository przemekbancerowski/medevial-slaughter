#ifndef CASTLE_h
#define CASTLE_h

#include "globalHeaders.h"

class castle : public building
{
public:
	static const int GOLD_INCOME = 10;
public:
	castle(void);
	castle(texture buildingTexture, int x, int y, buildingType type) : building(buildingTexture, x, y, type){}
	~castle(void);

	virtual int getIncome(); //TODO wyliczanie na podstawie wczytywanych modyfikatorow itp. z pliku
};

#endif