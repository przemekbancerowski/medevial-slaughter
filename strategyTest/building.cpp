#include "globalHeaders.h"


building::building(void)
{
}

building::building(texture buildingTexture, int x, int y, buildingType type)
{
	this->x = x;
	this->y = y;

	this->type = type;
	this->buildingTexture = buildingTexture;

	this->owner = 0;
	selected = false;
}

buildingType building::getType()
{
	return type;
}

void building::setX(int x)
{
	this->x = x;
}

void building::setY(int y)
{
	this->y = y;
}


building::~building(void)
{
}

void building::setSelection(bool select)
{
	selected = select;
}

bool building::isSelected()
{
	return selected;
}

texture building::getTexture()
{
	return buildingTexture;
}

int building::getX()
{
	return x;
}

int building::getY()
{
	return y;
}

player* building::getOwner()
{
	return owner;
}

void building::setOwner(player *owner)
{
	this->owner = owner;
}

int building::getIncome() 
{ 
	return 0; 
}