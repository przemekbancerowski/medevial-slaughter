#ifndef GUIBUTTON_h
#define GUIBUTTON_h
#include "globalHeaders.h"

class guiButton : public hgeGUIButton
{
public:
	guiButton(int _id, float x, float y, float w, float h, HTEXTURE tex, float tx, float ty) : hgeGUIButton(_id,x,y,w,h,tex,tx,ty) { lastButtonState = false; spr = new hgeSprite(tex, 0, 0, w, h); this->x = x; this->y = y; mouseOver = false; }
	bool clicked();
	void MouseOver(bool bOver);
	void Render();
private:
	bool lastButtonState;
	bool mouseOver;
	hgeSprite* spr;

	float x;
	float y;
};

#endif