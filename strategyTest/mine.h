#ifndef MINE_h
#define MINE_h

#include "globalHeaders.h"

class mine :public building
{
public:
	static const int GOLD_INCOME_PER_WORKER = 2;

	mine(void);
	mine(texture buildingTexture, int x, int y, buildingType type) : building(buildingTexture, x, y, type){ owner = 0; workers = 0; resources = 728; }
	~mine(void);

	void setWorkers(int workers);
	void setResources(int resources);

	int getWorkers();
	virtual int getIncome(); //TODO wyliczanie na podstawie wczytywanych modyfikatorow itp. z pliku
	int getResources();

private:
	int workers;
	int resources;
};

#endif

