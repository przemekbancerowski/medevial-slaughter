#ifndef GLOBAL_HEADERS_h
#define GLOBAL_HEADERS_h

//System
#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <ctime>
#include <cmath>

//HgeEngine
#include <hge.h>
#include <hgesprite.h>
#include <hgegui.h>
#include <hgeguictrls.h>
#include <hgeresource.h>

//Custom
#include "enumerators.h"
#include "guiButton.h"

#include "menu.h"
#include "mainMenu.h"

#include "unit.h"
#include "building.h"
#include "player.h"
#include "mine.h"
#include "castle.h"

#include "tile.h"
#include "map.h"

#include "game.h"




#endif
