#include "globalHeaders.h"
#include <string>

game::game(hgeResourceManager* resourceManager, HGE* hge)
{
	this->resourceManager = resourceManager;

	this->hge = hge;
	gui = new hgeGUI();
	
	//rekrutacja peasanta
	gui->AddCtrl(new guiButton(1, 100, 720, 64, 64, resourceManager->GetTexture("peasantButtonTex", 1), 0, 0));
	gui->GetCtrl(1)->bVisible = false;
	//rekrutacja piechura
	gui->AddCtrl(new guiButton(2, 180, 720, 64, 64, resourceManager->GetTexture("footmanButtonTex", 1), 0, 0));
	gui->GetCtrl(2)->bVisible = false;
	//rekrutacja lucznika
	gui->AddCtrl(new guiButton(3, 260, 720, 64, 64, resourceManager->GetTexture("archerButtonTex", 1), 0, 0));
	gui->GetCtrl(3)->bVisible = false;
	//rekrutacja pikiniera
	gui->AddCtrl(new guiButton(4, 340, 720, 64, 64, resourceManager->GetTexture("pikemanButtonTex", 1), 0, 0));
	gui->GetCtrl(4)->bVisible = false;
	//rekrutacja rycerza
	gui->AddCtrl(new guiButton(5, 420, 720, 64, 64, resourceManager->GetTexture("knightButtonTex", 1), 0, 0));
	gui->GetCtrl(5)->bVisible = false;

	
	//zamiana tury
	gui->AddCtrl(new guiButton(10, 900, 720, 64, 64, resourceManager->GetTexture("endTurnButtonTex", 1), 0, 0));

	//podziel jednostke
	gui->AddCtrl(new guiButton(11, 500, 720, 64, 64, resourceManager->GetTexture("splitUnitButtonTex", 1), 0, 0));
	gui->GetCtrl(11)->bVisible = false;
	//budowanie mostu
	gui->AddCtrl(new guiButton(14, 600, 720, 64, 64, resourceManager->GetTexture("buildBridgeButtonTex", 1), 0, 0));
	gui->GetCtrl(14)->bVisible = false;
	
	//potwierdz podzial
	gui->AddCtrl(new guiButton(12, 610, 370, 64, 64, resourceManager->GetTexture("confirmButtonTex", 1), 0, 0));

	//potwierdz kupno
	gui->AddCtrl(new guiButton(15, 610, 370, 64, 64, resourceManager->GetTexture("confirmButtonTex", 1), 0, 0));

	//slider
	gui->AddCtrl(new hgeGUISlider(13, 560, 352, 100, 20, resourceManager->GetTexture("buttonTex", 0), 0, 0, 5, 15));


	gui->GetCtrl(12)->bVisible = false;
	gui->GetCtrl(13)->bVisible = false;
	gui->GetCtrl(15)->bVisible = false;

	gameMap = new map(20, 11);

	isSplitted = false;
	splitCount = 0;

	splitDialogOpened = false;
	recruitDialogOpened = false;

	isBuilded = false;
	isRecruited = false;
}

game::~game(void)
{
	delete resourceManager;
	delete gui;
	delete gameMap;
}

void game::updateGUI(float dt)
{
	gui->Update(dt);
}

void game::handleGui()
{
	if(static_cast<guiButton*>(gui->GetCtrl(1))->clicked())
	{
		recruitDialog(new unit(PEASANT_SPR, PEASANT, 0, 0, 1));
	}
	else if(static_cast<guiButton*>(gui->GetCtrl(2))->clicked())
	{
		recruitDialog(new unit(FOOTMAN_SPR, FOOTMAN, 0, 0, 1));
	}
	else if(static_cast<guiButton*>(gui->GetCtrl(3))->clicked())
	{
		recruitDialog(new unit(ARCHER_SPR, ARCHER, 0, 0, 1));
	}
	else if(static_cast<guiButton*>(gui->GetCtrl(4))->clicked())
	{
		recruitDialog(new unit(PIKEMAN_SPR, PIKEMAN, 0, 0, 1));
	}
	else if(static_cast<guiButton*>(gui->GetCtrl(5))->clicked())
	{
		recruitDialog(new unit(KNIGHT_SPR, KNIGHT, 0, 0, 1));
	}
	else if(static_cast<guiButton*>(gui->GetCtrl(10))->clicked())
	{
		gameMap->switchTurn();
		
		splitDialogOpened = false;
		recruitDialogOpened = false;

		isBuilded = false;
		isRecruited = false;

		gui->GetCtrl(15)->bVisible = false;
		gui->GetCtrl(13)->bVisible = false;
	}
	else if(static_cast<guiButton*>(gui->GetCtrl(11))->clicked())
	{
		splitDialog();
	}
	else if(static_cast<guiButton*>(gui->GetCtrl(14))->clicked())
	{
		isBuilded = true;
	}
	else if(static_cast<guiButton*>(gui->GetCtrl(12))->clicked())
	{
		splitCount = (int)static_cast<hgeGUISlider*>(gui->GetCtrl(13))->GetValue();
		gui->GetCtrl(12)->bVisible = false;
		gui->GetCtrl(13)->bVisible = false;
		isSplitted = true;
		splitDialogOpened = false;
		gui->GetCtrl(12)->bEnabled = false;
	}
	else if(static_cast<guiButton*>(gui->GetCtrl(15))->clicked())
	{
		recruitCount = (int)static_cast<hgeGUISlider*>(gui->GetCtrl(13))->GetValue();
		gui->GetCtrl(15)->bVisible = false;
		gui->GetCtrl(13)->bVisible = false;
		gui->GetCtrl(15)->bEnabled = false;
		isRecruited = true;
		recruitDialogOpened = false;
	}
}

void game::recruitDialog(unit* unitObject)
{
	building* tmpBuilding = gameMap->getSelectedBuilding();
	player* player = 0;
	if(tmpBuilding != 0)
	{
		if(tmpBuilding->getType() == CASTLE)
		{
			if(gameMap->currentPlayer() == PLAYER_ONE)
				player = gameMap->getPlayerOne();
			else
				player = gameMap->getPlayerTwo();

			if(player->getGold() >= unitObject->getGoldCost())
			{
				if(unitObject->getType() == PEASANT)
				{
					static_cast<hgeGUISlider*>(gui->GetCtrl(13))->SetMode(0, 1, HGESLIDER_SLIDER);
					static_cast<hgeGUISlider*>(gui->GetCtrl(13))->SetValue(1);
				}
				else
				{
					float tmp = floor(player->getGold() / unitObject->getGoldCost() + 0.5f);
					static_cast<hgeGUISlider*>(gui->GetCtrl(13))->SetMode(0, tmp, HGESLIDER_SLIDER);
					float tmp2 = floor((player->getGold() / unitObject->getGoldCost())/2.0f + 0.5f);
					static_cast<hgeGUISlider*>(gui->GetCtrl(13))->SetValue(tmp2);
				}
				gui->GetCtrl(15)->bVisible = true;
				gui->GetCtrl(13)->bVisible = true;
				gui->GetCtrl(15)->bEnabled = true;
				recruitDialogOpened = true;
				unitToBeRecruited = unitObject;
			}
			else
				delete unitObject;
		}
		else
			delete unitObject;
	}
	else
	{
		delete unitObject;
	}
}

void game::splitDialog()
{
	//for(int j=0; j < gameMap->getMapHeight() ; j++)
 //   {
	//	for(int i=0; i < gameMap->getMapWidth(); i++)
 //       {
	//		if(gameMap->getTile(i,j).isSelected() == true)
	//		{
	//			if(gameMap->getTile(i,j).getUnit() != 0)
	//			{
	//				if(gameMap->getTile(i,j).getUnit()->isSelected() == true)
	//				{
	//					tmpUnit = gameMap->getTile(i,j).getUnit();
	//				}
	//			}
	//		}
	//	}
	//}

	unit* tmpUnit = gameMap->getSelectedUnit();
	if(tmpUnit != 0)
	{
		static_cast<hgeGUISlider*>(gui->GetCtrl(13))->SetMode(0, tmpUnit->getCount()-1, HGESLIDER_SLIDER);
		static_cast<hgeGUISlider*>(gui->GetCtrl(13))->SetValue(tmpUnit->getCount()/2);
		gui->GetCtrl(12)->bVisible = true;
		gui->GetCtrl(13)->bVisible = true;
		gui->GetCtrl(12)->bEnabled = true;
		splitDialogOpened = true;
	}
}

void game::renderSplitDialog()
{
	gameMap->setFocus(false);
	resourceManager->GetSprite("dialogBgSprite")->RenderEx(SCREEN_WIDTH/2 - 200, SCREEN_HEIGHT/2 - 200, 0, 5, 5);

	std::stringstream tmpStr;
	tmpStr << (int)static_cast<hgeGUISlider*>(gui->GetCtrl(13))->GetValue();
	resourceManager->GetFont("calibri")->Render(560, 330, HGETEXT_RIGHT, tmpStr.str().c_str() );
}

void game::renderRecruitDialog()
{
	gameMap->setFocus(false);
	resourceManager->GetSprite("dialogBgSprite")->RenderEx(SCREEN_WIDTH/2 - 200, SCREEN_HEIGHT/2 - 200, 0, 5, 5);

	std::stringstream tmpStr;
	tmpStr << (int)static_cast<hgeGUISlider*>(gui->GetCtrl(13))->GetValue();
	resourceManager->GetFont("calibri")->Render(560, 330, HGETEXT_RIGHT, tmpStr.str().c_str() );
}

void game::renderBottomPanel()
{
	//display info about buildings
	resourceManager->GetSprite("bottomPanelSprite")->Render(0, 704);
	if(gameMap->getSelectedBuilding() != 0)
	{
		if(gameMap->getSelectedBuilding()->getType() == MINE)
		{
			resourceManager->GetFont("calibri")->SetColor(ARGB(255,255,255,255)); 
			resourceManager->GetFont("calibri")->Render(100, 720, HGETEXT_LEFT, "Tutaj beda jakies bzdury o kopalni");
		}
		else if(gameMap->getSelectedBuilding()->getType() == CASTLE) 
		{
			//display gold status
			resourceManager->GetFont("calibri")->SetColor(ARGB(255,0,0,0));
			std::stringstream goldStatus;
			goldStatus << "Gold: ";
			player* currentPlayer;

			if(gameMap->currentPlayer() == PLAYER_ONE)
				currentPlayer = gameMap->getPlayerOne();
			else
				currentPlayer = gameMap->getPlayerTwo();

			if(gameMap->getSelectedBuilding()->getOwner() == currentPlayer)
			{
				goldStatus << currentPlayer->getGold();
				resourceManager->GetFont("calibri")->Render(20, 720, HGETEXT_LEFT, goldStatus.str().c_str());
			}
		}
	}
	
	if(gameMap->getSelectedUnit() != 0)
	{
		if(gameMap->getPlayerOne()->hasUnit(gameMap->getSelectedUnit()) && gameMap->currentPlayer() == PLAYER_ONE)
		{
			if(gameMap->getSelectedUnit()->getCount() > 1)
				gui->GetCtrl(11)->bVisible = true;

			if(gameMap->getSelectedUnit()->getType() == PEASANT)
			{
				gui->GetCtrl(14)->bVisible = true;
			}
			else 
				gui->GetCtrl(14)->bVisible = false;
		}
		else if(gameMap->getPlayerTwo()->hasUnit(gameMap->getSelectedUnit()) && gameMap->currentPlayer() == PLAYER_TWO)
		{
			gui->GetCtrl(11)->bVisible = true;

			if(gameMap->getSelectedUnit()->getType() == PEASANT)
			{
				gui->GetCtrl(14)->bVisible = true;
			}
			else 
				gui->GetCtrl(14)->bVisible = false;
		}


		resourceManager->GetFont("calibri")->SetColor(ARGB(255,255,255,255)); 
		resourceManager->GetFont("calibri")->Render(100, 720, HGETEXT_LEFT, "Tutaj beda jakies bzdury o jednostce");
	}
	else
	{
		gui->GetCtrl(11)->bVisible = false;
		gui->GetCtrl(14)->bVisible = false;
	}

	player* currentPlayer;
	if(gameMap->currentPlayer() == PLAYER_ONE)
				currentPlayer = gameMap->getPlayerOne();
			else
				currentPlayer = gameMap->getPlayerTwo();

	if(gameMap->getSelectedBuilding() != 0)
	{
		building* selectedBuilding = gameMap->getSelectedBuilding();
		if(selectedBuilding->getType() == CASTLE && selectedBuilding->getOwner() == currentPlayer)
		{
			gui->GetCtrl(1)->bVisible = true;
			gui->GetCtrl(2)->bVisible = true;
			gui->GetCtrl(3)->bVisible = true;
			gui->GetCtrl(4)->bVisible = true;
			gui->GetCtrl(5)->bVisible = true;

			gui->GetCtrl(1)->bEnabled = true;
			gui->GetCtrl(2)->bEnabled = true;
			gui->GetCtrl(3)->bEnabled = true;
			gui->GetCtrl(4)->bEnabled = true;
			gui->GetCtrl(5)->bEnabled = true;
		}
		else
		{
			gui->GetCtrl(1)->bVisible = false;
			gui->GetCtrl(2)->bVisible = false;
			gui->GetCtrl(3)->bVisible = false;
			gui->GetCtrl(4)->bVisible = false;
			gui->GetCtrl(5)->bVisible = false;

			gui->GetCtrl(1)->bEnabled = false;
			gui->GetCtrl(2)->bEnabled = false;
			gui->GetCtrl(3)->bEnabled = false;
			gui->GetCtrl(4)->bEnabled = false;
			gui->GetCtrl(5)->bEnabled = false;
		}
	}
	else
	{
		gui->GetCtrl(1)->bVisible = false;
		gui->GetCtrl(2)->bVisible = false;
		gui->GetCtrl(3)->bVisible = false;
		gui->GetCtrl(4)->bVisible = false;
		gui->GetCtrl(5)->bVisible = false;

		gui->GetCtrl(1)->bEnabled = false;
		gui->GetCtrl(2)->bEnabled = false;
		gui->GetCtrl(3)->bEnabled = false;
		gui->GetCtrl(4)->bEnabled = false;
		gui->GetCtrl(5)->bEnabled = false;
	}
}


void game::render()
{
	mapRender();
	rangeRender();
	attackRangeRender();

	if(splitDialogOpened) renderSplitDialog();
	if(recruitDialogOpened) renderRecruitDialog();
	if(isRecruited) renderRecruitRange();
	renderBottomPanel();
	gui->Render();

	handleCursor();
}

void game::renderRecruitRange()
{
	player* owner;
	if(gameMap->currentPlayer() == PLAYER_ONE)
		owner = gameMap->getPlayerOne();
	else
		owner = gameMap->getPlayerTwo();

	building* b = gameMap->getSelectedBuilding();
	if(b != 0)
	{
		if(b->getOwner() == owner)
		{
			if(b->getType() == CASTLE)
			{
				for(int j = b->getY()-1; j <= b->getY()+1; j++)
				{
					for(int i = b->getX()-1; i <= b->getX()+1; i++)
					{
						if(i >= 0 && i < gameMap->getMapWidth() && j >= 0 && j < gameMap->getMapHeight())
						{
							if(i == b->getX() && j == b->getY())
								continue;
							else
								resourceManager->GetSprite("rangeSprite")->Render(i*TILE_SIZE, j*TILE_SIZE);
						}
					}
				}
			}
		}
	}
}

void game::mouseHandler(float mouseX, float mouseY, bool LMB_state, bool LMB_clicked, bool RMB_clicked)
{
	this->mouseX = mouseX;
	this->mouseY = mouseY;
	this->LMB_state = LMB_state;
	this->LMB_clicked = LMB_clicked;
	this->RMB_clicked = RMB_clicked;
}

void game::handleCursor()
{	
	if(isSplitted)
	{
		if(gameMap->getSplitCursorStatus())
		{
			resourceManager->GetSprite("splitCursor")->Render(mouseX, mouseY);
		}
		else
		{
			resourceManager->GetSprite("pointerCursor")->Render(mouseX, mouseY);
		}
	}
	else if(gameMap->getMergeCursorStatus())
	{
		resourceManager->GetSprite("mergeCursor")->Render(mouseX, mouseY);
	}
	else if(gameMap->getAttackCursorStatus())
	{
		resourceManager->GetSprite("attackCursor")->Render(mouseX, mouseY);
		//pole na ktore sie stanie po ataku
		resourceManager->GetSprite("attackStandSprite")->Render(gameMap->getAttackStand().x*TILE_SIZE,gameMap->getAttackStand().y*TILE_SIZE);
	}
	else
	{
		resourceManager->GetSprite("pointerCursor")->Render(mouseX /*- resourceManager->GetSprite("pointerCursor")->GetWidth()*/, mouseY /*- resourceManager->GetSprite("pointerCursor")->GetHeight()*/);
	}
}

void game::handleMusic()
{
	if(!hge->Channel_IsPlaying(music))
	{
		music = hge->Stream_Play(resourceManager->GetStream("soundtrack", 1), true, 50);
	}
}

void game::handleGame()
{
	if(LMB_clicked && !splitDialogOpened && !recruitDialogOpened)
	{
		if(isRecruited == true && unitToBeRecruited != 0) delete unitToBeRecruited;
		if(isSplitted) isSplitted = false;
		if(isBuilded) isBuilded = false;
		if(isRecruited) isRecruited = false;
		int x = mouseX/TILE_SIZE;
		int y = mouseY/TILE_SIZE;
		gameMap->setSelect(x, y);
		if(gameMap->getSelectedUnit() != 0) 
		{
			selectedUnitRange = gameMap->getUnitRange();
			selectedUnitAttackRange = gameMap->getUnitAttackRange();
		}
	}
	if(RMB_clicked && !splitDialogOpened && !recruitDialogOpened)
	{
		if (mouseX >= 0 && mouseX < 1280 && mouseY >= 0 && mouseY < 704)
		{
			int x = mouseX/TILE_SIZE;
			int y = mouseY/TILE_SIZE;
		
		//TODO
		
			if(isSplitted)
			{
				gameMap->splitInteract(x, y, splitCount);
				isSplitted = false;
				if(gameMap->getSelectedUnit() != 0)
				{
					selectedUnitRange = gameMap->getUnitRange();
					selectedUnitAttackRange = gameMap->getUnitAttackRange();
				}
			}
			else if(isBuilded)
			{
				gameMap->buildInteract(x, y, BRIDGE);
				isBuilded = false;
				if(gameMap->getSelectedUnit() != 0)
				{
					selectedUnitRange = gameMap->getUnitRange();
					selectedUnitAttackRange = gameMap->getUnitAttackRange();
				}
			}
			else if(isRecruited)
			{
				unitToBeRecruited->setCount(recruitCount);
				gameMap->recruitUnit(unitToBeRecruited, x, y);
				isRecruited = false;
				unitToBeRecruited = 0;
				if(gameMap->getSelectedUnit() != 0)
				{
					selectedUnitRange = gameMap->getUnitRange();
					selectedUnitAttackRange = gameMap->getUnitAttackRange();
				}
			}
			else
			{
				gameMap->interact(x, y);
				if(gameMap->getSelectedUnit() != 0)
				{
					selectedUnitRange = gameMap->getUnitRange();
					selectedUnitAttackRange = gameMap->getUnitAttackRange();
				}
			}
		}
		
	}
}

void game::handleMap()
{
	gameMap->handler();
	gameMap->handleCoords(mouseX, mouseY);
}

void game::rangeRender()
{
	int x = 0;
	int y = 0;

	if(gameMap->getSelectedUnit() != 0)
	{
		for(int i = 0; i < selectedUnitRange.size(); i++)
		{
			resourceManager->GetSprite("rangeSprite")->Render(selectedUnitRange.at(i).x * TILE_SIZE, selectedUnitRange.at(i).y * TILE_SIZE);
		}
	}
}

void game::attackRangeRender()
{
	int x = 0;
	int y = 0;

	if(gameMap->getSelectedUnit() != 0)
	{
		for(int i = 0; i < selectedUnitAttackRange.size(); i++)
		{
			resourceManager->GetSprite("attackRangeSprite")->Render(selectedUnitAttackRange.at(i).x * TILE_SIZE, selectedUnitAttackRange.at(i).y * TILE_SIZE);
		}
	}
}

void game::mapRender()
{
	for(int i = 0; i < gameMap->getMapHeight(); i++)
	{
		for(int j = 0; j < gameMap->getMapWidth(); j++)
		{
			tileRender(gameMap->getTile(j,i));
		}
	}
}

void game::tileRender(tile& tileObject)
{
	if(tileObject.getTexture() == GRASS) resourceManager->GetSprite("groundSprite")->Render(tileObject.getX()*TILE_SIZE, tileObject.getY()*TILE_SIZE);
	else if(tileObject.getTexture() == RIVER) resourceManager->GetSprite("riverSprite")->Render(tileObject.getX()*TILE_SIZE, tileObject.getY()*TILE_SIZE);

	if(tileObject.getOnTileTexture() == FOREST) resourceManager->GetSprite("forestSprite")->Render(tileObject.getX()*TILE_SIZE, tileObject.getY()*TILE_SIZE);
	else if(tileObject.getOnTileTexture() == MOUTAIN) resourceManager->GetSprite("moutainSprite")->Render(tileObject.getX()*TILE_SIZE, tileObject.getY()*TILE_SIZE);

	if(tileObject.getBuilding() != 0) buildingRender(tileObject.getBuilding());

	if(tileObject.getUnit() != 0) unitRender(tileObject.getUnit());

	if(tileObject.isSelected() == true) resourceManager->GetSprite("selectionSprite")->Render(tileObject.getX()*TILE_SIZE, tileObject.getY()*TILE_SIZE);
}


void game::unitRender(unit* unitObject)
{
	if(gameMap->getPlayerOne()->hasUnit(unitObject))
	{
		if(unitObject->getTexture() == KNIGHT_SPR) resourceManager->GetSprite("knightSprite")->Render(unitObject->getX()*TILE_SIZE, unitObject->getY()*TILE_SIZE);
		else if(unitObject->getTexture() == PEASANT_SPR) resourceManager->GetSprite("peasantSprite")->Render(unitObject->getX()*TILE_SIZE, unitObject->getY()*TILE_SIZE);
		else if(unitObject->getTexture() == FOOTMAN_SPR) resourceManager->GetSprite("footmanSprite")->Render(unitObject->getX()*TILE_SIZE, unitObject->getY()*TILE_SIZE);
		else if(unitObject->getTexture() == PIKEMAN_SPR) resourceManager->GetSprite("pikemanSprite")->Render(unitObject->getX()*TILE_SIZE, unitObject->getY()*TILE_SIZE);
		else if(unitObject->getTexture() == ARCHER_SPR) resourceManager->GetSprite("archerSprite")->Render(unitObject->getX()*TILE_SIZE, unitObject->getY()*TILE_SIZE);
	}
	else if(gameMap->getPlayerTwo()->hasUnit(unitObject))
	{
		if(unitObject->getTexture() == KNIGHT_SPR) resourceManager->GetSprite("enemyKnightSprite")->Render(unitObject->getX()*TILE_SIZE, unitObject->getY()*TILE_SIZE);
		else if(unitObject->getTexture() == PEASANT_SPR) resourceManager->GetSprite("enemyPeasantSprite")->Render(unitObject->getX()*TILE_SIZE, unitObject->getY()*TILE_SIZE);
		else if(unitObject->getTexture() == FOOTMAN_SPR) resourceManager->GetSprite("enemyFootmanSprite")->Render(unitObject->getX()*TILE_SIZE, unitObject->getY()*TILE_SIZE);
		else if(unitObject->getTexture() == PIKEMAN_SPR) resourceManager->GetSprite("enemyPikemanSprite")->Render(unitObject->getX()*TILE_SIZE, unitObject->getY()*TILE_SIZE);
		else if(unitObject->getTexture() == ARCHER_SPR) resourceManager->GetSprite("enemyArcherSprite")->Render(unitObject->getX()*TILE_SIZE, unitObject->getY()*TILE_SIZE);
	}


	std::stringstream tmpStr;
	tmpStr << unitObject->getCount();
	resourceManager->GetFont("calibri")->SetColor(ARGB(255,255,255,255));
	resourceManager->GetFont("calibri")->Render((unitObject->getX()*TILE_SIZE)+60, (unitObject->getY()*TILE_SIZE)+45, HGETEXT_RIGHT, tmpStr.str().c_str() );

	std::stringstream tmpStr2;
	tmpStr2 << unitObject->getLevel();
	resourceManager->GetFont("calibri")->Render((unitObject->getX()*TILE_SIZE)+20, (unitObject->getY()*TILE_SIZE)+45, HGETEXT_RIGHT, tmpStr2.str().c_str() );
}

void game::buildingRender(building* buildingObject)
{
	if(buildingObject->getTexture() == CASTLE_SPR) resourceManager->GetSprite("castleSprite")->Render(buildingObject->getX()*TILE_SIZE, buildingObject->getY()*TILE_SIZE);
	else if(buildingObject->getTexture() == BRIDGE_SPR) resourceManager->GetSprite("bridgeSprite")->Render(buildingObject->getX()*TILE_SIZE, buildingObject->getY()*TILE_SIZE);
	else if(buildingObject->getTexture() == MINE_SPR) 
	{
		resourceManager->GetSprite("mineSprite")->Render(buildingObject->getX()*TILE_SIZE, buildingObject->getY()*TILE_SIZE);

	int owner = 0;
	if(static_cast<mine*>(buildingObject)->getOwner() == gameMap->getPlayerOne())
	{
		owner = 1;
	}
	else if (static_cast<mine*>(buildingObject)->getOwner() == gameMap->getPlayerTwo())
	{
		owner = 2;
	}
	else
	{
		owner = 0;
	}
	std::stringstream tmpStr;
	tmpStr << owner << " - " << static_cast<mine*>(buildingObject)->getWorkers();

	resourceManager->GetFont("calibri")->SetColor(ARGB(255,255,255,255));
	resourceManager->GetFont("calibri")->Render((buildingObject->getX()*TILE_SIZE)+60, (buildingObject->getY()*TILE_SIZE)+45, HGETEXT_RIGHT, tmpStr.str().c_str() );
	}
}

map* game::getGameMap()
{
	return gameMap;
}