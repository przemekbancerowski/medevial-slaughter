#ifndef BUILDING_h
#define BUILDING_h
#include "globalHeaders.h"

class player;

class building
{
public:
	building(void);
	building(texture buildingTexture, int x, int y, buildingType type);
	~building(void);

	int getX();
	int getY();
	buildingType getType();
	texture getTexture();
	virtual int getIncome();
	player* getOwner();

	void setSelection(bool select);
	void setX(int x);
	void setY(int y);
	void setOwner(player* owner);

	bool isSelected();

private:
	bool selected;
	int x;
	int y;
	texture buildingTexture;
	buildingType type;

protected:
	player* owner;
};

#endif