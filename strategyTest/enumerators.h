#ifndef ENUMERATORS_h
#define ENUMERATORS_h

#include "globalHeaders.h"

struct coords
{
	int x;
	int y;
};

enum turn
{
	PLAYER_ONE,
	PLAYER_TWO
};

enum texture
{
	EMPTY,
	TEST,
	GRASS,
	RIVER,
	FOREST,
	KNIGHT_SPR,
	PEASANT_SPR,
	FOOTMAN_SPR,
	ARCHER_SPR,
	PIKEMAN_SPR,
	CASTLE_SPR,
	MINE_SPR,
	BRIDGE_SPR,
	MOUTAIN
};

enum unitType
{
	PEASANT, 
	KNIGHT, 
	ARCHER,
	PIKEMAN,
	FOOTMAN
};

enum buildingType
{
	CASTLE,
	MINE,
	BRIDGE
};

#endif