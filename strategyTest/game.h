#ifndef GAME_h
#define GAME_h

#include "globalHeaders.h"

#define SCREEN_WIDTH 1280
#define SCREEN_HEIGHT 800
#define SCREEN_BPP 32
#define TILE_SIZE 64


class game
{
public:
	game(hgeResourceManager* resourceManager, HGE* hge);
	~game();
	
	void render();


	void attackRangeRender();
	void renderBottomPanel();
	void tileRender(tile& tileObject);
	void unitRender(unit* unitObject);
	void buildingRender(building* buildingObject);
	void mapRender();
	void rangeRender();
	map* getGameMap();
	void updateGUI(float dt);
	void handleGui();
	void splitDialog();
	void recruitDialog(unit* unitObject);
	void renderRecruitDialog();

	void renderSplitDialog();
	void handleMap();
	void handleGame();
	void handleMusic();
	void handleCursor();

	void renderRecruitRange();


	void mouseHandler(float mouseX, float mouseY, bool LMB_state, bool LMB_clicked, bool RMB_clicked);

private:
	float mouseX;
	float mouseY;
	bool LMB_state;
	bool LMB_clicked;
	bool RMB_clicked;

	std::vector<coords> selectedUnitRange;
	std::vector<coords> selectedUnitAttackRange;
	

	hgeResourceManager* resourceManager;
	hgeGUI* gui;
	HGE* hge;
	map* gameMap;

	bool splitDialogOpened;
	bool recruitDialogOpened;

	bool isSplitted;
	int splitCount;
	bool isBuilded;
	bool isRecruited;
	int recruitCount;
	unit* unitToBeRecruited;

	int lastX;
	int lastY;

	//soundtrack
	HCHANNEL music;
};

#endif