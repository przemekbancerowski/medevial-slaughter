#ifndef UNIT_h
#define UNIT_h

#include "globalHeaders.h"

class unit
{
public:
	unit(texture unitTexture, unitType type, int x, int y, int count, int attack = 1, int defence = 1, int attackRange = 1);
	~unit(void);

	bool isSelected();
	bool isFought();

	int getX() const;
	int getY() const;
	int getMovementRange() const;
	int getBaseMovementRange() const;
	int getCount() const;
	int getGoldCost() const;
	const int getAttack() const;
    const int getDefence() const;
    const int getLevel() const;
    const long getExperience() const;
	int getAttackRange() const;
	texture getTexture() const;
	unitType getType() const;
	bool isStackable() const;

	void setSelection(bool select);
	void setX(int x);
	void setY(int y);
	void setMovementRange(int range);
    void setLevel(int level);
    void setExperience(long experience);
    void setSkills(const int & level);
	void setFought(bool fought);
	void setCount(int count);

	void addExperience(const long & experience);
	void addCount();

	
private:
	bool selected;
	texture unitTexture;
	int x;
	int y;
	int movementRange;
	int baseMovementRange;
	int count;
	int attackRange;
	int attack;
    int defence;
    int level;
    long experience;
	bool fought;
	unitType type;
	std::map<std::string, int> factorsMap;

	void fillFactorsMap();
};

#endif