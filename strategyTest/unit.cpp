#include "globalHeaders.h"

unit::unit(texture unitTexture, unitType type, int x, int y, int count, int attack, int defence, int attackRange)
{
    selected = false;
    this->unitTexture = unitTexture;
    this->x = x;
    this->y = y;
    this->count = count;
    this->type = type;
    this->experience = 0;
    this->level = 1;
	this->fought = false;
	fillFactorsMap();

	//TODO temprorary, delete this as soon as possible
	movementRange = factorsMap.find("movementRange")->second;
}

int unit::getAttackRange() const
{
	return attackRange;
}

void unit::setCount(int count)
{
	this->count = count;
}

void unit::addCount()
{
	count++;
}

unitType unit::getType() const
{
	return type;
}

int unit::getCount() const
{
	return count;
}

void unit::setSelection(bool select)
{
	selected = select;
}

int unit::getX() const
{
	return x;
}

int unit::getY() const
{
	return y;
}

void unit::setX(int x)
{
	this->x = x;
}

int unit::getMovementRange() const
{
	return this->movementRange;
}

int unit::getBaseMovementRange() const
{
	return baseMovementRange;
}

void unit::setMovementRange(int range)
{
	this->movementRange = range;
}

void unit::setY(int y)
{
	this->y = y;
}
texture unit::getTexture() const
{
	return unitTexture;
}

bool unit::isFought()
{
	return fought;
}

void unit::setFought(bool fought)
{
	this->fought = fought;
}

bool unit::isSelected()
{
	return selected;
}

unit::~unit(void)
{
	factorsMap.clear();
}

int unit::getGoldCost() const
{
	return factorsMap.find("goldCost")->second;
}

const int unit::getAttack() const
{
    return attack;
}
 
const int unit::getDefence() const
{
    return defence;
}
 
const int unit::getLevel() const
{
    return level;
}
 
const long unit::getExperience() const
{
    return experience;
}
 
void unit::setLevel(int level)
{
    this->level = level;
}
 
void unit::setExperience(long experience)
{
    this->experience = experience;
}

void unit::setSkills(const int & level)
{
	//set level modifier
	double levelModifier = 0.0;
	switch(level)
	{
		case 1:
			levelModifier = 1.0;
			break;
		case 2:
			levelModifier = 1.2;
			break;
		case 3:
			levelModifier = 1.6;
			break;
	}

	//modify skills
	attack = (int) (levelModifier * (double)factorsMap["attack"]);
	defence = (int) (levelModifier * (double)factorsMap["defence"]);

	//add specific bonus on level 3
	if(level == 3)
	{
		switch(this->getType())
		{
		case KNIGHT:
			baseMovementRange += 1;
			break;
		case ARCHER:
			attackRange += 1;
			break;
		case FOOTMAN:
			defence += 2;
			break;
		case PIKEMAN:
			baseMovementRange += 1;
			break;
		}
	}
}
 
void unit::addExperience(const long & experience)
{
    this->experience += experience;
    if (this->experience >= 100 && this->experience < 300 && this->level < 2)
    {
        this->setLevel(2);
        this->setSkills(2);
    }
    else if (this->experience >= 300 && this->level < 3)
    {
        this->setLevel(3);
        this->setSkills(3);
    }
}

bool unit::isStackable() const
{
	if (this->factorsMap.find("isStackable")->second == 1)
		return true;
	else
		return false;
}

void unit::fillFactorsMap()
{
	//TODO loading the data from config files
	switch (this->getType()) 
	{
		case FOOTMAN:
			factorsMap["isStackable"] = 1;
			factorsMap["goldCost"] = 8;
			factorsMap["attack"] = 4;
			factorsMap["defence"] = 6;
			factorsMap["attackRange"] = 1;
			factorsMap["movementRange"] = 3;
			break;
		case PIKEMAN:
			factorsMap["isStackable"] = 1;
			factorsMap["goldCost"] = 12;
			factorsMap["attack"] = 6;
			factorsMap["defence"] = 4;
			factorsMap["attackRange"] = 1;
			factorsMap["movementRange"] = 2;
			break;
		case ARCHER:
			factorsMap["isStackable"] = 1;
			factorsMap["goldCost"] = 14;
			factorsMap["attack"] = 5;
			factorsMap["defence"] = 2;
			factorsMap["attackRange"] = 3;
			factorsMap["movementRange"] = 2;
			break;
		case KNIGHT:
			factorsMap["isStackable"] = 1;
			factorsMap["goldCost"] = 35;
			factorsMap["attack"] = 10;
			factorsMap["defence"] = 8;
			factorsMap["attackRange"] = 1;
			factorsMap["movementRange"] = 4;
			break;
		case PEASANT:
			factorsMap["isStackable"] = 0;
			factorsMap["goldCost"] = 10;
			factorsMap["attack"] = 1;
			factorsMap["defence"] = 1;
			factorsMap["attackRange"] = 1;
			factorsMap["movementRange"] = 2;
			break;
	}
	attack = factorsMap["attack"];
	defence = factorsMap["defence"];
	attackRange = factorsMap["attackRange"];
	baseMovementRange = factorsMap["movementRange"];
}
