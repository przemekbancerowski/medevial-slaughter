#ifndef MAP_h
#define MAP_h

#include "globalHeaders.h"
#define BRIDGE_GOLD_COST 25
class map
{
public:
	map(void);
	map(int width, int height);
	~map(void);

	void loadMap();
	void unselectAll();
	void setSelect(int x, int y);
	void interact(int x, int y);
	void splitInteract(int x, int y, int splitCount);
	void buildInteract(int x, int y, buildingType type);

	std::vector<coords> getUnitRange();

	std::vector<coords> getUnitAttackRange();

	turn currentPlayer();

	void initializePlayers();
	
	player* getPlayerOne();
	player* getPlayerTwo();

	double getAttackBonus(unitType attackingUnitType, unitType enemyUnitType);
	void fight(unit *attackingUnit, unit *enemyUnit);

	void switchPlayer();
	void switchTurn();

	void deployUnit(unit* standingUnit, player* owner = 0);
	void moveUnit(int x1, int y1, int x2, int y2);
	void recruitUnit(unit* unitObject, int x, int y);
	void setSplitCount(int unitCount);
	void splitUnit(unit* firstUnit, int splitCount, int x, int y, player* owner);

	void setBuilding(building* standingBuilding, player* owner = 0);

	building* getSelectedBuilding();

	void minesHandler(); //TODO rozbic na kilka metod

	void handler(); //TODO po prostu "handler"?

	unit* getSelectedUnit();

	int getMapWidth();
	int getMapHeight();
	tile& getTile(int x, int y);

	void mergeUnits(unit* firstUnit, unit* secondUnit, player* owner);

	void setFocus(bool focus);
	void checkCastle(player* playerObject);

	player* checkWin();

	void pathFinder(int x,int y,int **distance);
	int resolvePathCost(int x2, int y2);
	void generateIncome();
	void beginTurn();
	
	coords resolveShortestPath(int x2, int y2, int attackRange = 0);


	void handleMergeCursor();
	void handleCoords(float x, float y);
	bool getMergeCursorStatus();

	void handleSplitCursor();
	bool getSplitCursorStatus();

	void handleAttackCursor();
	bool getAttackCursorStatus();

	coords getAttackStand();
private:
    tile **tileMap;
    int mapWidth;
    int mapHeight;


	std::vector<building*> buildings;
	//nalezy zadac sobie pytanie czy robimy gui jakies u dolu basic, czy otwieramy dialoga z rekrutacja np
	bool hasFocus;
	bool unitIsSplitted;

	//gra
	player* playerOne;
	player* playerTwo;

	turn whoHasTurn;

	int splitCount;
	coords lastCoords;

	bool mergeCursor;
	bool splitCursor;
	bool attackCursor;

	coords attackStand;

};

#endif