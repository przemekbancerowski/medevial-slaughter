#include "globalHeaders.h"

map::map(void)
{
}

map::~map(void)
{
	delete playerOne;
	delete playerTwo;
}


map::map(int mapWidth, int mapHeight)
{
	int splitCount = 0;
	hasFocus = true;
	unitIsSplitted = false;
	this->mapWidth = mapWidth;
	this->mapHeight = mapHeight;

	tileMap = new tile*[mapWidth];
    for(int i=0;i<mapWidth;i++)
    {
        tileMap[i] = new tile[mapHeight];
    }
	loadMap();
		
	initializePlayers();

	lastCoords.x = 0;
	lastCoords.y = 0;

}

void map::setSplitCount(int unitCount)
{
	hasFocus = true;
	unitIsSplitted = true;
	splitCount = unitCount;
}

void map::initializePlayers()
{
	//player1
	playerOne = new player();

	//deployUnit(new unit(KNIGHT_SPR, KNIGHT, 7, 2, 100, 7, 5),  playerOne);
    //deployUnit(new unit(KNIGHT_SPR, KNIGHT, 6, 2, 100, 7, 5),  playerOne);
	
	setBuilding(new castle(CASTLE_SPR, 0,4, CASTLE), playerOne);

	//player2
	playerTwo = new player();

	//deployUnit(new unit(KNIGHT_SPR, KNIGHT, 12, 2, 100, 7, 5), playerTwo);
    //deployUnit(new unit(KNIGHT_SPR, KNIGHT, 13, 2, 100, 7, 5), playerTwo);

	setBuilding(new castle(CASTLE_SPR, 17, 0, CASTLE), playerTwo);

	//game
	whoHasTurn = PLAYER_ONE;
	beginTurn();
}

turn map::currentPlayer()
{
	return whoHasTurn;
}

void map::setFocus(bool focus)
{
	hasFocus = focus;
}

void map::setBuilding(building* buildingToSet, player* owner)
{
	if(tileMap[buildingToSet->getX()][buildingToSet->getY()].isStandable() == true)
	{
		buildingToSet->setOwner(owner);
		buildings.push_back(buildingToSet);
		tileMap[buildingToSet->getX()][buildingToSet->getY()].setBuilding(buildingToSet);
	}
}

void map::minesHandler()
{
	int workersCount = 0;
	mine* tmpMine = 0;
	for(int i = 0; i < buildings.size(); i++)
	{
		workersCount = 0;
		if(buildings.at(i)->getType() == MINE)
		{
			tmpMine = static_cast<mine*>(buildings.at(i));
			if(tmpMine->getWorkers() == 0)
			{
				tmpMine->setOwner(0);
			}
			for(int j = tmpMine->getY()-1; j <= tmpMine->getY()+1; j++)
			{
				for(int k = tmpMine->getX()-1; k <= tmpMine->getX()+1; k++)
				{
					if(j >= 0 && j < mapHeight && k >= 0 && k < mapWidth)
					{
						if(tileMap[k][j].getUnit() != 0 && tileMap[k][j].getUnit()->getType() == PEASANT)
						{
							if(tmpMine->getOwner() == 0)
							{
								if(playerOne->hasUnit(tileMap[k][j].getUnit()))
								{
									++workersCount;
									tmpMine->setOwner(playerOne);
								}
								else if (playerTwo->hasUnit(tileMap[k][j].getUnit()))
								{
									++workersCount;
									tmpMine->setOwner(playerTwo);
								}
								
							}
							else
							{
								if(tmpMine->getOwner()->hasUnit(tileMap[k][j].getUnit()))
								{
										++workersCount;
										tmpMine->setWorkers(workersCount);
								}
							}
						}
					}
				}
			}
			tmpMine->setWorkers(workersCount);
		}
	}
}

player* map::getPlayerOne()
{
	return playerOne;
}

player* map::getPlayerTwo()
{
	return playerTwo;
}

void map::switchPlayer()
{
	if(whoHasTurn == PLAYER_ONE)
	{
		whoHasTurn = PLAYER_TWO;

		for(int i = 0; i < playerTwo->getUnits().size(); i++)
		{
			playerTwo->getUnits().at(i)->setMovementRange(playerTwo->getUnits().at(i)->getBaseMovementRange());
			playerTwo->getUnits().at(i)->setFought(false);
		}
	}
	else
	{
		whoHasTurn = PLAYER_ONE;
		for(int i = 0; i < playerOne->getUnits().size(); i++)
		{
			playerOne->getUnits().at(i)->setMovementRange(playerOne->getUnits().at(i)->getBaseMovementRange());
			playerOne->getUnits().at(i)->setFought(false);
		}
	}
}

player* map::checkWin()
{
	if(playerOne->getTurnToLose() <= 0)
	{
		return playerTwo;
	}
	else if(playerTwo->getTurnToLose() <= 0)
	{
		return playerOne;
	}
	else
	{
		return 0;
	}
}

void map::switchTurn()
{	
	unselectAll();
	player *tmpPlayer;
	if(currentPlayer() == PLAYER_ONE)
	{
		tmpPlayer = playerOne;
	}
	else
	{
		tmpPlayer = playerTwo;
	}
	checkCastle(tmpPlayer);
	switchPlayer();
	beginTurn();
}

void map::generateIncome()
{
	int income = 0;
	player *tmpPlayer;
	if(currentPlayer() == PLAYER_ONE)
	{
		tmpPlayer = playerOne;
	}
	else if(currentPlayer() == PLAYER_TWO)
	{
		tmpPlayer = playerTwo;
	}
	for(int i = 0; i < mapHeight; i++)
	{
		for(int j = 0; j < mapWidth; j++)
		{
			building* tmpBuilding = tileMap[j][i].getBuilding();
			if(tmpBuilding)
			{
				if(tmpBuilding->getOwner() == tmpPlayer)
				{
					income += tmpBuilding->getIncome();
				}
			}
		}
	}
	tmpPlayer->earnGold(income);
}

void map::beginTurn()
{
	generateIncome();
}

void map::checkCastle(player* playerObject)
{
	for(int i = 0; i < mapHeight; i++)
	{
		for(int j = 0; j < mapWidth; j++)
		{
			building* tmpBuilding = tileMap[j][i].getBuilding();
			if(tmpBuilding)
			{
				if(tmpBuilding->getType() == CASTLE)
				{
					if(tmpBuilding->getOwner() == playerObject)
					{
						playerObject->setTurnToLose(3);
						return;
					}
				}
			}
		}
	}
	playerObject->setTurnToLose(playerObject->getTurnToLose() - 1);
}


void map::splitUnit(unit* firstUnit, int splitCount, int x, int y, player* owner)
{
	int cost = firstUnit->getMovementRange()-resolvePathCost(x, y);
	deployUnit(new unit(firstUnit->getTexture(), firstUnit->getType(), x, y, splitCount), owner); 
	firstUnit->setCount(firstUnit->getCount() - splitCount);
	unitIsSplitted = false;
	tileMap[x][y].getUnit()->setMovementRange(cost);
	unselectAll();
	tileMap[x][y].setSelect(true);
	tileMap[x][y].getUnit()->setLevel(firstUnit->getLevel());
	tileMap[x][y].getUnit()->setExperience(firstUnit->getExperience());
	tileMap[x][y].getUnit()->setFought(firstUnit->isFought());
}

void map::setSelect(int x, int y)
{
	if(x >= 0 && x <= mapWidth && y >= 0 && y < mapHeight)
	{
		unselectAll();
		tileMap[x][y].setSelect(true);

		if(whoHasTurn == PLAYER_ONE)
		{
			if(tileMap[x][y].getUnit() != 0)
			{
				if(playerTwo->hasUnit(tileMap[x][y].getUnit()))
				{
					tileMap[x][y].getUnit()->setSelection(false);
				}
			}
		}
		else if(whoHasTurn == PLAYER_TWO)
		{
			if(tileMap[x][y].getUnit() != 0)
			{
				if(playerOne->hasUnit(tileMap[x][y].getUnit()))
				{
					tileMap[x][y].getUnit()->setSelection(false);
				}
			}
		}
	}
}

void map::buildInteract(int x, int y, buildingType type)
{
	//get current player object
	player* tmpPlayer = 0;
	if(currentPlayer() == PLAYER_ONE)
		tmpPlayer = playerOne;
	else
		tmpPlayer = playerTwo;

	if(tmpPlayer->canAfford(BRIDGE_GOLD_COST)) 
	{
		if(tileMap[x][y].getTexture() == RIVER)
		{
			unit* tmpUnit = getSelectedUnit();
			if( tmpUnit != 0)
			{
				if(tmpUnit->getType() == PEASANT)
				{
					if(abs(tmpUnit->getX()-x) <= 1 && abs(tmpUnit->getY()-y) <= 1)
					{
						tileMap[x][y].setBuilding(new building(BRIDGE_SPR, x, y, BRIDGE));
						tileMap[x][y].setStandable(true);
						tmpPlayer->spendGold(BRIDGE_GOLD_COST);
					}
				}
			}
		}
	}
}

void map::splitInteract(int x, int y, int splitCount)
{
	unit* tmpUnit = getSelectedUnit();

	if(tmpUnit != 0)
	{
		player* owner = 0;
		if(whoHasTurn == PLAYER_ONE)
		{
			owner = playerOne;
		}
		else if(whoHasTurn == PLAYER_TWO)
		{
			owner = playerTwo;
		}
		std::vector<coords> tmp = getUnitRange();

		for(int i = 0; i < tmp.size(); i++)
		{
			if(tmp.at(i).x == x && tmp.at(i).y == y)
			{
				if(tileMap[x][y].isStandable())
				{
					if(splitCount != 0 && tmpUnit->getMovementRange() > 0)
					{
						splitUnit(tmpUnit, splitCount, x, y, owner);
					}
				}
			}
		}
	}
}

void map::interact(int x, int y)
{
	unit* tmpUnit = getSelectedUnit();

	if(tmpUnit != 0)
	{
		if(tileMap[x][y].isStandable() == true)
		{
			moveUnit(tmpUnit->getX(), tmpUnit->getY(), x, y);
		}

		if(whoHasTurn == PLAYER_ONE)
        {
            if(tileMap[x][y].getUnit() != 0)
            {
                if(playerOne->hasUnit(tileMap[x][y].getUnit()))
                {
					if(abs(tmpUnit->getX() - x) <= 1 && abs(tmpUnit->getY() - y) <= 1)
                    {
						if(tmpUnit->getMovementRange() >= 1)
							mergeUnits(tmpUnit, tileMap[x][y].getUnit(), playerOne);
					}
					else
					{
						coords tmpPoint = resolveShortestPath(x, y, 1);
						if(tmpUnit->getMovementRange() > resolvePathCost(tmpPoint.x, tmpPoint.y))
						{
							moveUnit(tmpUnit->getX(), tmpUnit->getY(), tmpPoint.x, tmpPoint.y);
							mergeUnits(tmpUnit, tileMap[x][y].getUnit(), playerOne);
						}
					}
                }
                else
                {
					if(abs(tmpUnit->getX() - x) <= tmpUnit->getAttackRange() && abs(tmpUnit->getY() - y) <= tmpUnit->getAttackRange())
                    {
						if(!tmpUnit->isFought())
							fight(tmpUnit, tileMap[x][y].getUnit());
					}
					else
					{
						coords tmpPoint = resolveShortestPath(x, y);
						if(tmpUnit->getMovementRange() >= resolvePathCost(tmpPoint.x, tmpPoint.y))
						{
							if(!tmpUnit->isFought())
							{
								moveUnit(tmpUnit->getX(), tmpUnit->getY(), tmpPoint.x, tmpPoint.y);
								fight(tmpUnit, tileMap[x][y].getUnit());
							}
						}
					}
                }
            }
			else if(tileMap[x][y].getBuilding() != 0)
			{
				if(tileMap[x][y].getBuilding()->getType() == CASTLE)
				{
					if(tileMap[x][y].getBuilding()->getOwner() == playerTwo)
					{
						if((abs(tmpUnit->getX() - tileMap[x][y].getBuilding()->getX()) <= 1 && abs(tmpUnit->getY() - tileMap[x][y].getBuilding()->getY()) <= 1))
							tileMap[x][y].getBuilding()->setOwner(playerOne);
					}
				}
			}
        }
        else if(whoHasTurn == PLAYER_TWO)
        {
            if(tileMap[x][y].getUnit() != 0)
            {
                if(playerTwo->hasUnit(tileMap[x][y].getUnit()))
                {
					if(abs(tmpUnit->getX() - x) <= 1 && abs(tmpUnit->getY() - y) <= 1)
                    {
						if(tmpUnit->getMovementRange() >= 1)
							mergeUnits(tmpUnit, tileMap[x][y].getUnit(), playerTwo);
					}
					else
					{
						coords tmpPoint = resolveShortestPath(x, y, 1);
						if(tmpUnit->getMovementRange() > resolvePathCost(tmpPoint.x, tmpPoint.y))
						{
							moveUnit(tmpUnit->getX(), tmpUnit->getY(), tmpPoint.x, tmpPoint.y);
							mergeUnits(tmpUnit, tileMap[x][y].getUnit(), playerTwo);
						}
					}
                }
                else
                {
					if(abs(tmpUnit->getX() - x) <= tmpUnit->getAttackRange() && abs(tmpUnit->getY() - y) <= tmpUnit->getAttackRange())
                    {
						if(!tmpUnit->isFought())
							fight(tmpUnit, tileMap[x][y].getUnit());
					}
					else
					{
						coords tmpPoint = resolveShortestPath(x, y);
						if(tmpUnit->getMovementRange() >= resolvePathCost(tmpPoint.x, tmpPoint.y))
						{
							if(!tmpUnit->isFought())
							{
								moveUnit(tmpUnit->getX(), tmpUnit->getY(), tmpPoint.x, tmpPoint.y);
								fight(tmpUnit, tileMap[x][y].getUnit());
							}
						}
					}
                }
            }
			else if(tileMap[x][y].getBuilding() != 0)
			{
				if(tileMap[x][y].getBuilding()->getType() == CASTLE)
				{
					if(tileMap[x][y].getBuilding()->getOwner() == playerOne)
					{
						if((abs(tmpUnit->getX() - tileMap[x][y].getBuilding()->getX()) <= 1 && abs(tmpUnit->getY() - tileMap[x][y].getBuilding()->getY()) <= 1))
							tileMap[x][y].getBuilding()->setOwner(playerTwo);
					}
				}
			}
        }
	}
		//flagi
	mergeCursor = false;
	splitCursor = false;
	attackCursor = false;
}

unit* map::getSelectedUnit()
{
	unit* tmpUnit = 0;
	for(int j=0; j < mapHeight ; j++)
    {
		for(int i=0; i < mapWidth; i++)
		{
			if(tileMap[i][j].isSelected() == true)
			{
				if(tileMap[i][j].getUnit() != 0)
				{
					if(tileMap[i][j].getUnit()->isSelected() == true)
					{
						tmpUnit = tileMap[i][j].getUnit();
					}
				}
			}
		}
	}

	return tmpUnit;
}

int map::resolvePathCost(int x2, int y2)
{
	int x1 = getSelectedUnit()->getX();
	int y1 = getSelectedUnit()->getY();
	int** distance;
	distance = new int*[getMapWidth()];
    for(int i=0;i<getMapWidth();i++)
    {
        distance[i] = new int[getMapHeight()];
    }
	for(int j=0;j<getMapHeight();++j)
	{
		for(int i=0;i<getMapWidth();++i)
		{
			if(!getTile(i,j).isStandable())
			{
				distance[i][j]=-1;
			}
			else
			{
				distance[i][j]=10000;
			}
		}
	}
	distance[x1][y1]=0;

	pathFinder(x1,y1,distance);

	//resolvePath
	int stepCount = 0;

	stepCount = distance[x2][y2];

	for(int i=0;i<getMapWidth();i++)
    {
        delete []distance[i];
    }
	delete []distance;

	return stepCount;
}

void map::handleSplitCursor()
{
	if(getSelectedUnit() != 0)
	{
		if(getSelectedUnit()->getMovementRange() >= resolvePathCost(lastCoords.x, lastCoords.y) && tileMap[lastCoords.x][lastCoords.y].isStandable())
			splitCursor = true;
		else
			splitCursor = false;
	}
	
}

bool map::getSplitCursorStatus()
{
	return splitCursor;
}

void map::handleCoords(float x, float y)
{
	if (x >= 0 && x < 1280 && y >= 0 && y < 704)
	{
		if(lastCoords.x != static_cast<int>(x/TILE_SIZE))
		{
			lastCoords.x = static_cast<int>(x/TILE_SIZE);
			handleMergeCursor();
			handleSplitCursor();
			handleAttackCursor();
		}
		if(lastCoords.y != static_cast<int>(y/TILE_SIZE))
		{
			lastCoords.y = static_cast<int>(y/TILE_SIZE);
			handleMergeCursor();
			handleSplitCursor();
			handleAttackCursor();
		}
	}
}

bool map::getMergeCursorStatus()
{
	return mergeCursor;
}

void map::handleMergeCursor()
{
	unit* selected = getSelectedUnit();
	unit* over = tileMap[lastCoords.x][lastCoords.y].getUnit();

	player* owner = 0;

	if(currentPlayer() == PLAYER_ONE)
		owner = playerOne;
	else 
		owner = playerTwo;

	if(selected != 0 && over != 0)
	{
		if(selected != over)
		{
			if(owner->hasUnit(selected) && owner->hasUnit(over))
			{
				if(selected->getType() == over->getType() && selected->getLevel() == over->getLevel())
				{
					coords tmpCoords = resolveShortestPath(over->getX(), over->getY(), 1);
					if(selected->getMovementRange() > resolvePathCost(tmpCoords.x, tmpCoords.y))
					{
						mergeCursor = true;
					}
					else
						mergeCursor = false;
				}
				else
					mergeCursor = false;
			}
			else 
				mergeCursor = false;
		}
		else
			mergeCursor = false;
	}
	else 
		mergeCursor = false;
}

void map::handleAttackCursor()
{
	unit* selected = getSelectedUnit();
	unit* over = tileMap[lastCoords.x][lastCoords.y].getUnit();

	player* owner = 0;

	if(currentPlayer() == PLAYER_ONE)
		owner = playerOne;
	else 
		owner = playerTwo;

	if(selected != 0 && over != 0)
	{
		if(selected != over)
		{
			if(owner->hasUnit(selected) && !owner->hasUnit(over))
			{
				if(!selected->isFought())
				{
					if(selected->getAttackRange() < abs(over->getX() - selected->getX()) || selected->getAttackRange() < abs(over->getY() - selected->getY()))
					{
						coords tmpCoords = resolveShortestPath(over->getX(), over->getY(), selected->getAttackRange());
						int cost = resolvePathCost(tmpCoords.x, tmpCoords.y);
						if(selected->getMovementRange() >= cost)
						{
							attackCursor = true;
							attackStand = tmpCoords;
						}
					}
					else
					{
						attackCursor = true;
						attackStand.x = selected->getX();
						attackStand.y = selected->getY();
					}
				}
				else
					attackCursor = false;
			}
			else
				attackCursor = false;
		}
		else 
			attackCursor = false;
	}
	else
		attackCursor = false;
}

bool map::getAttackCursorStatus()
{
	return attackCursor;
}

coords map::resolveShortestPath(int x2, int y2, int attackRange)
{
	if(attackRange <= 0) attackRange = getSelectedUnit()->getAttackRange();

	int x1 = getSelectedUnit()->getX();
	int y1 = getSelectedUnit()->getY();
	int** distance;
	distance = new int*[getMapWidth()];
    for(int i=0;i<getMapWidth();i++)
    {
        distance[i] = new int[getMapHeight()];
    }
	for(int j=0;j<getMapHeight();++j)
	{
		for(int i=0;i<getMapWidth();++i)
		{
			if(!getTile(i,j).isStandable())
			{
				distance[i][j]=-1;
			}
			else
			{
				distance[i][j]=10000;
			}
		}
	}
	distance[x1][y1]=0;

	pathFinder(x1,y1,distance);

	//resolvePath
	int minimum = 10000;
	coords result;
	result.x = 0;
	result.y = 0;
	for(int i = 0; i < 2*attackRange+1; i++)
	{
		for(int j = 0; j < 2*attackRange+1; j++)
		{
			if(i + x2 - attackRange >= 0 && i +x2 - attackRange < mapWidth && j + y2 - attackRange >= 0 && j + y2 - attackRange < mapHeight)
			{
				if(distance[x2+i-attackRange][y2+j-attackRange] > 0)
				{
					if(distance[x2+i-attackRange][y2+j-attackRange] < minimum)
					{
						minimum = distance[x2+i-attackRange][y2+j-attackRange];
						result.x = x2+i-attackRange;
						result.y = y2+j-attackRange;
					}
				}
			}
		}
	}

	for(int i=0;i<getMapWidth();i++)
    {
        delete []distance[i];
    }
	delete []distance;

	
	return result;
}

std::vector<coords> map::getUnitAttackRange()
{
	std::vector<coords> tmpCoord;
	int x = getSelectedUnit()->getX();
	int y = getSelectedUnit()->getY();

	player* owner;
	if(currentPlayer() == PLAYER_ONE)
		owner = playerOne;
	else
		owner = playerTwo;

	if(!getSelectedUnit()->isFought())
	{
		int aRange = getTile(x, y).getUnit()->getAttackRange();
		for(int j = y-getTile(x, y).getUnit()->getMovementRange()-aRange; j <= y + getTile(x, y).getUnit()->getMovementRange()+aRange ; j++)
		{
			for(int i = x-getTile(x, y).getUnit()->getMovementRange()-aRange; i <= x + getTile(x, y).getUnit()->getMovementRange()+aRange ; i++)
			{
				if((j >= 0 && j < getMapHeight())&&(i >= 0 && i < getMapWidth()))
				{
					if(getTile(i,j).getUnit() != 0)
					{
						if(!owner->hasUnit(getTile(i,j).getUnit()))
						{
							coords standCoord = resolveShortestPath(i, j, getSelectedUnit()->getAttackRange());
							if(getSelectedUnit()->getMovementRange()+getSelectedUnit()->getAttackRange() >= resolvePathCost(standCoord.x, standCoord.y))
							{
								coords tmp;
								tmp.x = i;
								tmp.y = j;
								tmpCoord.push_back(tmp);
							}
						}
					}
				}
			}
		}
	}

	return tmpCoord;
}

std::vector<coords> map::getUnitRange()
{
	std::vector<coords> tmpCoord;
	int x = getSelectedUnit()->getX();
	int y = getSelectedUnit()->getY();
	int **distance;//zwolnic
	distance = new int*[getMapWidth()];
    for(int i=0;i<getMapWidth();i++)
    {
        distance[i] = new int[getMapHeight()];
    }
	for(int j=0;j<getMapHeight();++j)
	{
		for(int i=0;i<getMapWidth();++i)
		{
			if(!getTile(i,j).isStandable())
			{
				distance[i][j]=-1;
			}
			else
			{
				distance[i][j]=10000;
			}
		}
	}
	distance[x][y]=0;
	pathFinder(x,y,distance);
	for(int j = y-getTile(x, y).getUnit()->getMovementRange(); j <= y + getTile(x, y).getUnit()->getMovementRange() ; j++)
	{
		for(int i = x-getTile(x, y).getUnit()->getMovementRange(); i <= x + getTile(x, y).getUnit()->getMovementRange() ; i++)
		{
			if((j >= 0 && j < getMapHeight())&&(i >= 0 && i < getMapWidth()))
			{
				if(((distance[i][j]<=getTile(x, y).getUnit()->getMovementRange()))&&(distance[i][j]>0))
				{
					coords tmp;
					tmp.x = i;
					tmp.y = j;
					tmpCoord.push_back(tmp);
				}
			}
		}
	}
	for(int i=0;i<getMapWidth();i++)
    {
        delete []distance[i];
    }
	delete []distance;
	return tmpCoord;
}

void map::pathFinder(int x,int y,int **distance)
{
	int tmp;
	for(int j=-1;j<=1;++j)
		for(int i=-1;i<=1;++i)
			if(((i!=0)||(j!=0))&&((y+j) >= 0 && (y+j) < getMapHeight())&&((x+i) >= 0 && (x+i) < getMapWidth())&&(distance[x+i][y+j]>distance[x][y]))
			{
				tmp=distance[x][y];
				if(getTile(x+i,y+j).getOnTileTexture()==FOREST)
					tmp+=2;
				else
					++tmp;
				if(tmp<distance[x+i][y+j])
				{
					distance[x+i][y+j]=tmp;
					pathFinder(x+i,y+j,distance);
				}
			}
}

building* map::getSelectedBuilding()
{
	building* tmpBuilding = 0;
	for(int j=0; j < mapHeight ; j++)
    {
		for(int i=0; i < mapWidth; i++)
		{
			if(tileMap[i][j].isSelected() == true)
			{
				if(tileMap[i][j].getBuilding() != 0)
				{
					if(tileMap[i][j].getBuilding()->isSelected() == true)
					{
						tmpBuilding = tileMap[i][j].getBuilding();
					}
				}
			}
		}
	}

	return tmpBuilding;
}

void map::handler()
{
	minesHandler();
}

void map::mergeUnits(unit* firstUnit, unit* secondUnit, player* owner)
{
	if(firstUnit != secondUnit)
	{
		if(firstUnit->getType() == secondUnit->getType())
		{
			if(firstUnit->getType() != PEASANT)
			{
				if(firstUnit->getLevel() == secondUnit->getLevel())
				{
					secondUnit->setCount(firstUnit->getCount() + secondUnit->getCount());
					for(int z = 0; z < owner->getUnits().size(); z++)
					{
						if(firstUnit == owner->getUnits().at(z))
						{
							coords tmp = resolveShortestPath(secondUnit->getX(), secondUnit->getY(), 1);
							firstUnit->setMovementRange(firstUnit->getMovementRange() - resolvePathCost(tmp.x, tmp.y));
							if(secondUnit->getMovementRange() > firstUnit->getMovementRange())
								secondUnit->setMovementRange(firstUnit->getMovementRange());
							else 
								secondUnit->setMovementRange(secondUnit->getMovementRange());

							//walczenie
							if(secondUnit->isFought() || firstUnit->isFought())
								secondUnit->setFought(true);
							else
								secondUnit->setFought(false);
							

							//srednia wazona expa
							double tmpExp = (firstUnit->getCount() * firstUnit->getExperience() + secondUnit->getExperience() * secondUnit->getExperience())/(firstUnit->getCount() + secondUnit->getCount());
							secondUnit->setExperience(floor(tmpExp+0.5));
							unselectAll();
							tileMap[secondUnit->getX()][secondUnit->getY()].setSelect(true);

							tileMap[firstUnit->getX()][firstUnit->getY()].freeUnit();
							delete firstUnit;
							owner->getUnits().erase(owner->getUnits().begin()+z);
						}
					}
				}
			}
		}
	}
}

double map::getAttackBonus(unitType attackingUnitType, unitType enemyUnitType)
{
    // attack bonus 
	double BONUS = 0.0;

	if (attackingUnitType == KNIGHT && enemyUnitType == FOOTMAN)
	{ 
	  	BONUS = 0.3; 
	} 
	else if (attackingUnitType == PIKEMAN && enemyUnitType == KNIGHT)
	{ 
	  	BONUS = 0.4;
	}
	else if (attackingUnitType == FOOTMAN && enemyUnitType == PIKEMAN)
	{
		BONUS = 0.5;
	}

	return BONUS;
}
 
void map::fight(unit *attackingUnit, unit *enemyUnit)
{
	int attackingAttack = attackingUnit->getAttack(); 
	int attackingCount = attackingUnit->getCount(); 
	unitType attackingType = attackingUnit->getType(); 
	int enemyDefence = enemyUnit->getDefence(); 
	int enemyCount = enemyUnit->getCount(); 
	unitType enemyType = enemyUnit->getType(); 

	double random; 
	// getting ready rand() function because it does not work properly with just one call (always returns very similar number) 
	for (int i = 0; i < 2; ++i) 
	{
		random = (double) rand() / ((double) RAND_MAX * 10.0); 
	}
	
	// counting killed units 
	int killedUnitsCount = 0;
	const double BONUS = getAttackBonus(attackingType, enemyType); 
  	const double INITIAL_CHANCE = 0.2;
  	bool bonusGiven = false;
  	// while there are units to attack and enemy is not dead
  	while (attackingCount > 0 && enemyCount > 0)
  	{ 		
  		double attackChance = INITIAL_CHANCE;
  		
  		// if attacking units have advantage over enemys'
  		if (attackingAttack > enemyDefence || BONUS > 0.0)
  		{
	  		attackChance +=  0.1 * ((double) attackingAttack / (double) enemyDefence);
	  	}
	  	// else if enemys' units have advantage over attackings'
	  	else if (attackingAttack < enemyDefence)
	  	{
	  		attackChance -=  0.1 * ((double) attackingAttack / (double) enemyDefence);
	  	}
	  	
	  	attackChance += BONUS;

	  	// if attack chances were successful in a try, kill one enemy's unit
	  	if (((double) rand() / (double) RAND_MAX) <= attackChance)
	  	{
	  		--enemyCount;
	  		++killedUnitsCount;
	  	}
	  	
	  	// if attack chances are 50%+ and enemys has count advantage
	  	if (attackChance >= 0.5 && attackingUnit->getCount() < enemyUnit->getCount() && !bonusGiven)
	  	{
	  		int countDiff = enemyUnit->getCount() - attackingUnit->getCount();
	  		//add countDiff / 2 bonus attacks
	  		attackingCount += floor((double) countDiff * 0.5 + 0.5);
	  		bonusGiven = true;
		}
		// else if attack chances are 50%+ and attacking has count advantage
		else if (attackChance >= 0.5 && attackingUnit->getCount() > enemyUnit->getCount() && !bonusGiven)
		{
			int countDiff = attackingUnit->getCount() - enemyUnit->getCount();
			// gives countDiff times a following chance to bonus attack
			for (int i = 0; i < countDiff; ++i)
			{
				// with INITIAL CHANCE (20%) + (chance / 2)% probability a unit gets a bonus attack
		  		if (((double) rand() / (double) RAND_MAX) <= (INITIAL_CHANCE + (attackChance * 0.5)))
		  			++attackingCount;
		  	}
		  	bonusGiven = true;
		}
		else
			--attackingCount;
  	}
  	
	///////////////////////////////////////
	//remove killed enemy's units 
	enemyUnit->setCount(enemyCount); 
	//add experience equal to count of killed units 
	attackingUnit->addExperience(killedUnitsCount);
	//set flag for fought to true
	attackingUnit->setFought(true);	

    // if enemy dies
    if (enemyUnit->getCount() <= 0) 
    { 
        // for every unit belonging to player two
        for(int z = 0; z < playerTwo->getUnits().size(); z++)
        { 
            // if dying unit belongs to player two - delete it
            if(enemyUnit == playerTwo->getUnits().at(z)) 
            { 
                tileMap[enemyUnit->getX()][enemyUnit->getY()].freeUnit(); 
                delete enemyUnit; 
                playerTwo->getUnits().erase(playerTwo->getUnits().begin() + z);    
                break; 
            } 
        } 
        // for every unit belonging to player one
        for(int z = 0; z < playerOne->getUnits().size(); z++)
        { 
            // if dying unit belongs to player one - delete it
            if(enemyUnit == playerOne->getUnits().at(z)) 
            { 
                tileMap[enemyUnit->getX()][enemyUnit->getY()].freeUnit(); 
                delete enemyUnit; 
                playerOne->getUnits().erase(playerOne->getUnits().begin() + z); 
                break; 
            } 
        } 
    } 
} 

void map::recruitUnit(unit* unitObject, int x, int y)
{
	//find owner
	player *owner = NULL;
	if(whoHasTurn == PLAYER_ONE)
	{
		owner = playerOne;
	}
	else if(whoHasTurn == PLAYER_TWO)
	{
		owner = playerTwo;
	}

	if(unitObject->getCount() > 0)
	{
		building* castleBuilding = getSelectedBuilding();
		if(abs(castleBuilding->getX() - x ) <= 1 && abs(castleBuilding->getY() - y) <=  1)
		{
			if(tileMap[x][y].getUnit() != 0)
			{
				if(owner->hasUnit(tileMap[x][y].getUnit()))
				{
					if(unitObject->isStackable() && unitObject->getType() == tileMap[x][y].getUnit()->getType())
					{
						if(owner->canAffordUnit(unitObject))
						{
							tileMap[x][y].getUnit()->setCount(tileMap[x][y].getUnit()->getCount() + unitObject->getCount());
							owner->spendGold(unitObject->getGoldCost() * unitObject->getCount());
							delete unitObject;
						}				
					}
				}
			} 
			else if(tileMap[x][y].isStandable() == true)
			{
				if(owner->canAffordUnit(unitObject))
				{
					unitObject->setX(x);
					unitObject->setY(y);
					deployUnit(unitObject, owner);
					owner->spendGold(unitObject->getGoldCost() * unitObject->getCount());
				}
				else 
				{
					delete unitObject;
				}
			}
			else
				delete unitObject;
		}
	}
	else
		delete unitObject;
}

void map::deployUnit(unit* standingUnit, player* owner)
{
	if(tileMap[standingUnit->getX()][standingUnit->getY()].isStandable() == true)
	{
		if(owner != 0)
			owner->addUnit(standingUnit);

		tileMap[standingUnit->getX()][standingUnit->getY()].deployUnit(standingUnit);
	}
}

void map::unselectAll()
{
	for(int j=0; j < mapHeight ; j++)
    {
            for(int i=0; i < mapWidth; i++)
            {
				tileMap[i][j].setSelect(false);
				if(tileMap[i][j].getUnit())
				{
					tileMap[i][j].getUnit()->setSelection(false);
				}
				else if(tileMap[i][j].getBuilding())
				{
					tileMap[i][j].getBuilding()->setSelection(false);
				}
			}
	}


}

coords map::getAttackStand()
{
	return attackStand;
}

void map::moveUnit(int x1, int y1, int x2, int y2)
{
	unit* tmpUnit = tileMap[x1][y1].getUnit();

	std::vector<coords> tmpCoords = getUnitRange();
	bool found = false;

	for(int i =0; i < tmpCoords.size(); i++)
	{
		if(tmpCoords.at(i).x == x2 && tmpCoords.at(i).y == y2)
		{
			found = true;
			break;
		}
	}


	//tmpUnit->setMovementRange(tmpUnit->getMovementRange() - (abs(x2 - x1) + abs(y2 - y1)));
	if(found)
	{
		int stepCount = resolvePathCost(x2, y2);
		tmpUnit->setMovementRange(tmpUnit->getMovementRange() - stepCount);
		tileMap[x1][y1].freeUnit();
		tileMap[x2][y2].deployUnit(tmpUnit);
		unselectAll();
		tileMap[x2][y2].setSelect(true);
	}
}

void map::loadMap()
{
	for(int j=0; j < mapHeight ; j++)
        {
            for(int i=0; i < mapWidth; i++)
            {
				tileMap[i][j].setTilePos(i, j);
			}
	}

	
	getTile(0,10).setTexture(RIVER);
	getTile(0,10).setStandable(false);
	getTile(0,9).setTexture(RIVER);
	getTile(0,9).setStandable(false);
	getTile(1,9).setTexture(RIVER);
	getTile(1,9).setStandable(false);
	getTile(1,8).setTexture(RIVER);
	getTile(1,8).setStandable(false);

	getTile(2,8).setTexture(RIVER);
	getTile(2,8).setBuilding(new building(BRIDGE_SPR, 2, 8, BRIDGE));
	getTile(2,8).setStandable(true);

	getTile(3,8).setTexture(RIVER);
	getTile(3,8).setStandable(false);
	getTile(4,8).setTexture(RIVER);
	getTile(4,8).setStandable(false);
	getTile(5,8).setTexture(RIVER);
	getTile(5,8).setStandable(false);
	getTile(5,7).setTexture(RIVER);
	getTile(5,7).setStandable(false);
	getTile(6,7).setTexture(RIVER);
	getTile(6,7).setStandable(false);
	getTile(6,6).setTexture(RIVER);
	getTile(6,6).setStandable(false);
	getTile(7,6).setTexture(RIVER);
	getTile(7,6).setStandable(false);
	getTile(7,5).setTexture(RIVER);
	getTile(7,5).setStandable(false);
	getTile(8,5).setTexture(RIVER);
	getTile(8,5).setStandable(false);
	getTile(8,4).setTexture(RIVER);
	getTile(8,4).setStandable(false);
	getTile(8,3).setTexture(RIVER);
	getTile(8,3).setStandable(false);
	getTile(9,3).setTexture(RIVER);
	getTile(9,3).setStandable(false);

	getTile(9,5).setTexture(RIVER);
	getTile(9,5).setStandable(false);
	getTile(10,5).setTexture(RIVER);
	getTile(10,5).setStandable(false);
	getTile(11,5).setTexture(RIVER);
	getTile(11,5).setStandable(false);
	getTile(12,5).setTexture(RIVER);
	getTile(12,5).setStandable(false);

	getTile(13,0).setTexture(RIVER);
	getTile(13,0).setStandable(false);
	getTile(13,1).setTexture(RIVER);
	getTile(13,1).setStandable(false);
	getTile(14,0).setTexture(RIVER);
	getTile(14,0).setStandable(false);


	getTile(2,5).setOnTileTexture(FOREST);
	getTile(3,5).setOnTileTexture(FOREST);
	getTile(4,5).setOnTileTexture(FOREST);
	getTile(3,4).setOnTileTexture(FOREST);
	getTile(4,4).setOnTileTexture(FOREST);
	getTile(4,3).setOnTileTexture(FOREST);
	getTile(5,3).setOnTileTexture(FOREST);
	getTile(4,2).setOnTileTexture(FOREST);
	getTile(5,2).setOnTileTexture(FOREST);
	getTile(6,2).setOnTileTexture(FOREST);
	getTile(5,1).setOnTileTexture(FOREST);
	getTile(6,1).setOnTileTexture(FOREST);
	getTile(6,0).setOnTileTexture(FOREST);
	getTile(7,0).setOnTileTexture(FOREST);

	getTile(2,11).setOnTileTexture(FOREST);
	getTile(3,11).setOnTileTexture(FOREST);
	getTile(4,11).setOnTileTexture(FOREST);
	getTile(5,11).setOnTileTexture(FOREST);
	getTile(6,11).setOnTileTexture(FOREST);
	getTile(7,11).setOnTileTexture(FOREST);
	getTile(8,11).setOnTileTexture(FOREST);

	getTile(3,10).setOnTileTexture(FOREST);
	getTile(4,10).setOnTileTexture(FOREST);
	getTile(5,10).setOnTileTexture(FOREST);
	getTile(6,10).setOnTileTexture(FOREST);
	getTile(7,10).setOnTileTexture(FOREST);
	getTile(8,10).setOnTileTexture(FOREST);

	getTile(4,9).setOnTileTexture(FOREST);
	getTile(5,9).setOnTileTexture(FOREST);
	getTile(6,9).setOnTileTexture(FOREST);
	getTile(7,9).setOnTileTexture(FOREST);
	getTile(8,8).setOnTileTexture(FOREST);

	getTile(11,8).setOnTileTexture(FOREST);
	getTile(11,7).setOnTileTexture(FOREST);
	getTile(12,6).setOnTileTexture(FOREST);

	getTile(13,2).setOnTileTexture(FOREST);
	getTile(14,1).setOnTileTexture(FOREST);
	getTile(14,2).setOnTileTexture(FOREST);
	getTile(14,3).setOnTileTexture(FOREST);

	getTile(15,3).setOnTileTexture(FOREST);
	getTile(15,2).setOnTileTexture(FOREST);

	getTile(15,7).setOnTileTexture(FOREST);
	getTile(15,8).setOnTileTexture(FOREST);
	getTile(16,9).setOnTileTexture(FOREST);
	getTile(16,10).setOnTileTexture(FOREST);
	getTile(17,10).setOnTileTexture(FOREST);

	getTile(7,7).setOnTileTexture(MOUTAIN);
	getTile(7,7).setStandable(false);
	getTile(7,8).setOnTileTexture(MOUTAIN);
	getTile(7,8).setStandable(false);
	getTile(6,8).setOnTileTexture(MOUTAIN);
	getTile(6,8).setStandable(false);
	getTile(7,9).setOnTileTexture(MOUTAIN);
	getTile(7,9).setStandable(false);
	getTile(8,9).setOnTileTexture(MOUTAIN);
	getTile(8,9).setStandable(false);

	getTile(9,9).setOnTileTexture(MOUTAIN);
	getTile(9,9).setStandable(false);
	getTile(10,9).setOnTileTexture(MOUTAIN);
	getTile(10,9).setStandable(false);

	getTile(10,8).setOnTileTexture(MOUTAIN);
	getTile(10,8).setStandable(false);
	getTile(10,7).setOnTileTexture(MOUTAIN);
	getTile(10,7).setStandable(false);

	getTile(12,8).setOnTileTexture(MOUTAIN);
	getTile(12,8).setStandable(false);
	getTile(12,9).setOnTileTexture(MOUTAIN);
	getTile(12,9).setStandable(false);
	getTile(12,10).setOnTileTexture(MOUTAIN);
	getTile(12,10).setStandable(false);
	getTile(13,10).setOnTileTexture(MOUTAIN);
	getTile(13,10).setStandable(false);

	getTile(14,7).setOnTileTexture(MOUTAIN);
	getTile(14,7).setStandable(false);
	getTile(14,6).setOnTileTexture(MOUTAIN);
	getTile(14,6).setStandable(false);
	getTile(14,5).setOnTileTexture(MOUTAIN);
	getTile(14,5).setStandable(false);
	getTile(13,5).setOnTileTexture(MOUTAIN);
	getTile(13,5).setStandable(false);
	getTile(13,4).setOnTileTexture(MOUTAIN);
	getTile(13,4).setStandable(false);

	getTile(10,0).setOnTileTexture(MOUTAIN);
	getTile(10,0).setStandable(false);
	getTile(11,0).setOnTileTexture(MOUTAIN);
	getTile(11,0).setStandable(false);
	getTile(12,0).setOnTileTexture(MOUTAIN);
	getTile(12,0).setStandable(false);

	getTile(12,1).setOnTileTexture(MOUTAIN);
	getTile(12,1).setStandable(false);
	getTile(11,1).setOnTileTexture(MOUTAIN);
	getTile(11,1).setStandable(false);
	getTile(11,2).setOnTileTexture(MOUTAIN);
	getTile(11,2).setStandable(false);

	getTile(16,6).setOnTileTexture(MOUTAIN);
	getTile(16,6).setStandable(false);

	getTile(16, 7).setOnTileTexture(MOUTAIN);
	getTile(16, 7).setStandable(false);
	getTile(17, 7).setOnTileTexture(MOUTAIN);
	getTile(17, 7).setStandable(false);
	getTile(18, 7).setOnTileTexture(MOUTAIN);
	getTile(18, 7).setStandable(false);
	
	getTile(16, 8).setOnTileTexture(MOUTAIN);
	getTile(16, 8).setStandable(false);
	getTile(17, 8).setOnTileTexture(MOUTAIN);
	getTile(17, 8).setStandable(false);
	getTile(18, 8).setOnTileTexture(MOUTAIN);
	getTile(18, 8).setStandable(false);
	getTile(19, 8).setOnTileTexture(MOUTAIN);
	getTile(19, 8).setStandable(false);
	getTile(17, 9).setOnTileTexture(MOUTAIN);
	getTile(17, 9).setStandable(false);
	getTile(18, 9).setOnTileTexture(MOUTAIN);
	getTile(18, 9).setStandable(false);
	getTile(19, 9).setOnTileTexture(MOUTAIN);
	getTile(19, 9).setStandable(false);

	getTile(18, 10).setOnTileTexture(MOUTAIN);
	getTile(18, 10).setStandable(false);
	getTile(19, 10).setOnTileTexture(MOUTAIN);
	getTile(19, 10).setStandable(false);
	/**/

	setBuilding(new mine(MINE_SPR, 3, 1, MINE), 0);
	setBuilding(new mine(MINE_SPR, 8, 7, MINE), 0);
	setBuilding(new mine(MINE_SPR, 16, 4, MINE), 0);
}

int map::getMapWidth()
{
	return mapWidth;
}

int map::getMapHeight()
{
	return mapHeight;
}
	
tile& map::getTile(int x, int y)
{
	return tileMap[x][y];
}
