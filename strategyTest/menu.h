#ifndef MENU_h
#define MENU_h

#include "globalHeaders.h"

class menu
{
public:
	menu(hgeResourceManager* resourceManager);
	~menu(void);
	virtual void render() = 0;
	virtual void handler(float dt) = 0;


protected:
	hgeResourceManager* resourceManager;
	hgeGUI* gui;

	float mouseX;
	float mouseY;
	bool LMB_state;
	bool LMB_clicked;
	bool RMB_clicked;
};

#endif