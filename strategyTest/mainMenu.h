#ifndef MAINMENU_h
#define MAINMENU_h

#include "globalHeaders.h"

class mainMenu :
	public menu
{
public:
	mainMenu(hgeResourceManager* resourceManager) : menu(resourceManager) 
	{
		newGame = false;

		gui->AddCtrl(new guiButton(1, 600, 300, 261, 33, resourceManager->GetTexture("newGameButtonTex"), 0, 0));
	}
	void render();
	void handler(float dt);

	bool newGameClicked();
	void setNewGameClicked(bool flag);

	void mouseHandler(float mouseX, float mouseY, bool LMB_state, bool LMB_clicked, bool RMB_clicked);

	~mainMenu(void);

private:
	bool newGame;
};

#endif