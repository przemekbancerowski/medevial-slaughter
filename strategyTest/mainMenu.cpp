#include "globalHeaders.h"

mainMenu::~mainMenu(void)
{

}

void mainMenu::handler(float dt)
{
	if(static_cast<guiButton*>(gui->GetCtrl(1))->clicked())
	{
		newGame = true;
	}

	gui->Update(dt);
}

void mainMenu::setNewGameClicked(bool flag)
{
	newGame = flag;
}

bool mainMenu::newGameClicked()
{
	return newGame;
}

void mainMenu::render()
{
	gui->Render();
	resourceManager->GetSprite("pointerCursor")->Render(mouseX, mouseY);
}

void mainMenu::mouseHandler(float mouseX, float mouseY, bool LMB_state, bool LMB_clicked, bool RMB_clicked)
{
	this->mouseX = mouseX;
	this->mouseY = mouseY;
	this->LMB_state = LMB_state;
	this->LMB_clicked = LMB_clicked;
	this->RMB_clicked = RMB_clicked;
}