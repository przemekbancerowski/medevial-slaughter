#include "globalHeaders.h"

tile::tile(void)
{
	this->x = 1;
	this->y = 1;

	this->tileTexture = GRASS;
	this->onTileTexture = EMPTY;

	selected = false;
	standable = true;

	standingUnit = 0;
	standingBuilding = 0;
}

tile::~tile(void)
{
	
}

void tile::setTilePos(int x, int y)
{
	setX(x);
	setY(y);
}

int tile::getX()
{
	return x;
}

int tile::getY()
{
	return y;
}

bool tile::isStandable()
{
	return standable;
}

void tile::setStandable(bool standable)
{
	this->standable = standable;
}

void tile::setTexture(texture tileTexture)
{
	this->tileTexture = tileTexture;
}

void tile::setOnTileTexture(texture onTileTexture)
{
	this->onTileTexture = onTileTexture;
}

void tile::deployUnit(unit* standingUnit)
{
	if(this->standable == true)
	{
		standingUnit->setX(this->x);
		standingUnit->setY(this->y);
		this->standingUnit = standingUnit;
		this->standable = false;
	}
}

void tile::setBuilding(building* standingBuilding)
{
	this->standingBuilding = standingBuilding;
	this->standingBuilding->setX(this->x);
	this->standingBuilding->setY(this->y);
	this->standable = false;
}

void tile::freeUnit()
{
	this->standingUnit = 0;
	this->standable = true;
}

void tile::setX(int x)
{
	this->x = x;
}

void tile::setY(int y)
{
	this->y = y;
}

texture tile::getOnTileTexture()
{
	return onTileTexture;
}

texture tile::getTexture()
{
	return tileTexture;
}


unit* tile::getUnit()
{
	return standingUnit;
}


bool tile::isSelected()
{
	return selected;
}

void tile::setSelect(bool selection)
{
	if(this->standingUnit != 0)
	{
		standingUnit->setSelection(selection);
	}

	if(this->standingBuilding != 0)
	{
		standingBuilding->setSelection(selection);
	}
	
	selected = selection;
}

building* tile::getBuilding()
{
	return standingBuilding;
}