#ifndef PLAYER_h
#define PLAYER_h

#include "globalHeaders.h"

class player
{
public:
	player(void);
	~player(void);

	std::vector<unit*>& getUnits();
	void addUnit(unit* newUnit);
	bool hasUnit(unit* unitObject);
	void earnGold(int goldEarned);
	void spendGold(int goldSpent);
	int getGold();
	bool canAffordUnit(unit *unitObject);
	void setTurnToLose(int amount);
	int getTurnToLose();
	bool canAfford(int cost);
private:
	std::vector<unit*> units;
	int gold;
	int turnToLose;
};

#endif
