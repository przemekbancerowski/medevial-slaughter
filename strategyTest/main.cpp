#include "globalHeaders.h"

HGE* hge = 0;
game* gameObject = 0;
hgeResourceManager* resourceManager = 0;

mainMenu* mainMenuObject = 0;

float mouseX;
float mouseY;
bool LMB_state;
bool LMB_clicked;
bool RMB_clicked;

bool MainMenuFrameFunc();
bool MainMenuRenderFunc();

bool FrameFunc()
{
	float dt = hge->Timer_GetDelta();
	LMB_state = hge->Input_GetKeyState(HGEK_LBUTTON);
	LMB_clicked = hge->Input_KeyDown(HGEK_LBUTTON);
	RMB_clicked = hge->Input_KeyDown(HGEK_RBUTTON);
	hge->Input_GetMousePos(&mouseX,&mouseY);
	
	gameObject->mouseHandler(mouseX, mouseY, LMB_state, LMB_clicked, RMB_clicked);
	gameObject->handleGame();
	gameObject->handleMap();
	gameObject->handleGui();
	gameObject->handleMusic();
	gameObject->updateGUI(dt);

	if(gameObject->getGameMap()->checkWin() != 0)
	{
		hge->System_SetState(HGE_FRAMEFUNC, MainMenuFrameFunc);
		hge->System_SetState(HGE_RENDERFUNC, MainMenuRenderFunc);
	}
	
	return false;
}

bool RenderFunc()
{
	hge->Gfx_BeginScene();

	hge->Gfx_Clear(0xFFFFFFFF);

	gameObject->render();

	resourceManager->GetFont("calibri")->printf(10, 10, 0, "%d", hge->Timer_GetFPS());

	hge->Gfx_EndScene();

	return false;
}

bool MainMenuFrameFunc()
{
	float dt = hge->Timer_GetDelta();
	LMB_state = hge->Input_GetKeyState(HGEK_LBUTTON);
	LMB_clicked = hge->Input_KeyDown(HGEK_LBUTTON);
	RMB_clicked = hge->Input_KeyDown(HGEK_RBUTTON);
	hge->Input_GetMousePos(&mouseX,&mouseY);

	mainMenuObject->mouseHandler(mouseX, mouseY, LMB_state, LMB_clicked, RMB_clicked);
	mainMenuObject->handler(dt);

	if(mainMenuObject->newGameClicked())
	{
		mainMenuObject->setNewGameClicked(false);
		hge->System_SetState(HGE_FRAMEFUNC, FrameFunc);
		hge->System_SetState(HGE_RENDERFUNC, RenderFunc);
	}

	return false;
}

bool MainMenuRenderFunc()
{
	hge->Gfx_BeginScene();

	hge->Gfx_Clear(0xFFFFFFFF);

	mainMenuObject->render();

	hge->Gfx_EndScene();

	return false;
}

int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	hge = hgeCreate(HGE_VERSION);

	hge->System_SetState(HGE_FRAMEFUNC, MainMenuFrameFunc);
	hge->System_SetState(HGE_RENDERFUNC, MainMenuRenderFunc);
	

	hge->System_SetState(HGE_TITLE, "Strategy 2D");

	hge->System_SetState(HGE_SCREENWIDTH, SCREEN_WIDTH);
	hge->System_SetState(HGE_SCREENHEIGHT, SCREEN_HEIGHT);
	hge->System_SetState(HGE_SCREENBPP, 32);
	
	hge->System_SetState(HGE_WINDOWED, true);
	hge->System_SetState(HGE_HIDEMOUSE, true);

	hge->System_SetState(HGE_USESOUND, true);
	hge->System_SetState(HGE_SHOWSPLASH, false);
	hge->System_SetState(HGE_FPS,HGEFPS_VSYNC);



	if(hge->System_Initiate())
	{
		//init
		resourceManager = new hgeResourceManager("strategyResources.txt");

		mainMenuObject = new mainMenu(resourceManager);
		gameObject = new game(resourceManager, hge);
		gameObject->getGameMap()->loadMap();
		//initEnd

		//init rand
		srand(time(NULL));

		hge->System_Start();
	}
	else
	{	
		MessageBox(NULL, hge->System_GetErrorMessage(), "Error", MB_OK | MB_ICONERROR | MB_APPLMODAL);
	}
	hge->System_Shutdown();
	hge->Release();

	return 0;
}
