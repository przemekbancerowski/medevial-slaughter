#ifndef TILE_h
#define TILE_h

#include "globalHeaders.h"

class tile
{
public:
	tile(void);
	void setTilePos(int x, int y);
	~tile(void);

	int getY();
	int getX();

	void setX(int x);
	void setY(int y);

	void setTexture(texture tileTexture);
	void setOnTileTexture(texture onTileTexture);

	void setStandable(bool standable);

	void deployUnit(unit* standingUnit);
	void setBuilding(building* standingBuilding);

	texture getTexture();
	texture getOnTileTexture();

	bool isSelected();
	unit* getUnit();
	building* getBuilding();
	void freeUnit();
	bool isStandable();

	void setSelect(bool selection);
private:
	int x;
	int y;

	texture tileTexture;
	texture onTileTexture;

	unit* standingUnit;
	building* standingBuilding;
	
	bool selected;
	bool standable;
};

#endif
