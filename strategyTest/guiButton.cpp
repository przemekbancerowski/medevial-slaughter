#include "globalHeaders.h"

bool guiButton::clicked()
{
	if(GetState())
	{
		lastButtonState = GetState();
	}
	else
	{
		if(lastButtonState)
		{
			lastButtonState = false;
			return true;
		}

		lastButtonState = false;
	}

	return false;
}

void guiButton::MouseOver(bool bOver)
{
	mouseOver = bOver;
}

void guiButton::Render()
{
	if(mouseOver && !GetState())
	{
		spr->SetColor(ARGB(100, 255, 255, 255));
		spr->Render(x,y);
	}
	else
		hgeGUIButton::Render();
}