#include "globalHeaders.h"


mine::mine(void)
{
	workers = 0;
	owner = 0;
}


mine::~mine(void)
{
}

void mine::setWorkers(int workers)
{
	this->workers = workers;
}

int mine::getWorkers()
{
	return workers;
}

int mine::getIncome()
{
	int goldIncome = 0;
	if (this->resources > 0)
	{
		switch(getWorkers())
		{
			case 1:
				goldIncome = 5;
				break;
			case 2:
				goldIncome = 10;
				break;
			case 3:
				goldIncome = 14;
				break;
			case 4:
				goldIncome = 18;
				break;
			case 5:
				goldIncome = 22;
				break;
			case 6:
				goldIncome = 25;
				break;
			case 7:
				goldIncome = 28;
				break;
			case 8:
				goldIncome = 30;
				break;
		}
		resources -= goldIncome;
	}
	return goldIncome;
}

void mine::setResources(int resources)
{
	this->resources = resources;
}

int mine::getResources()
{
	return resources;
}