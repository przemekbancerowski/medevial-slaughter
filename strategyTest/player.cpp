#include "globalHeaders.h"

player::player(void)
{
	gold = 0;
	turnToLose = 3;
}

std::vector<unit*>& player::getUnits()
{
	return units;
}

bool player::hasUnit(unit* unitObject)
{
	for(int i = 0; i < units.size(); i++)
	{
		if(unitObject == units.at(i))
		{
			return true;
		}
	}

	return false;
}

int player::getTurnToLose()
{
	return turnToLose;
}

void player::setTurnToLose(int amount)
{
	turnToLose = amount;
}

void player::addUnit(unit* newUnit)
{
	units.push_back(newUnit);
}

void player::earnGold(int goldEarned)
{
	gold += goldEarned;
}

void player::spendGold(int goldSpent)
{
	gold -= goldSpent;
}
	
int player::getGold()
{
	return gold;
}

bool player::canAfford(int cost)
{
	if(this->gold >= cost)
		return true;
	else
		return false;
}

bool player::canAffordUnit(unit *unitObject)
{
	if(unitObject != 0)
	{
		if (getGold() >= unitObject->getGoldCost() * unitObject->getCount()) 
			return true;
		else
			return false;
	}
}

player::~player(void)
{
}
